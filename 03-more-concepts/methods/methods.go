package main

import (
	"fmt"
	"time"
)

type Person struct {
	First string
	Last  string
	Dob   time.Time
}

func (p Person) SayHello() {
	fmt.Printf("Hello, %s!\n", p.First)
}

func (p Person) GetAge() int {
	return int(time.Since(p.Dob).Hours() / 24 / 365)
}

func NewPerson(first, last string, year, month, day int) *Person {
	return &Person{
		First: first,
		Last:  last,
		Dob:   time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local),
	}
}

func main() {
	sayHello("John")
	s := getHello("Jane")
	fmt.Println(s)

	p := &Person{
		First: "John",
		Last:  "Doe",
	}
	p.SayHello()

	p = NewPerson("Jane", "Doe", 1980, 1, 1)
	p.SayHello()
	fmt.Println(p)
	fmt.Println(p.GetAge())
}

func sayHello(name string) {
	fmt.Printf("Hello, %s!\n", name)
}

func getHello(name string) string {
	return fmt.Sprintf("Hello, %s!\n", name)
}
