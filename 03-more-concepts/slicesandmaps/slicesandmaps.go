package main

import "fmt"

func main() {
	// slices
	var s []int
	fmt.Println(s)

	s = []int{1, 2, 3, 4}
	fmt.Println(s)
	s = append(s, 5)
	fmt.Println(s)

	s = make([]int, 0)
	fmt.Println(s)

	s = make([]int, 10)
	fmt.Println(s)
	s[0] = 5
	fmt.Println(s)

	a := [4]int{1, 2, 3, 4}
	fmt.Println(a)
	// a = append(a, 5) // will not compile, since list is a fixed size

	// maps
	m := map[string]string{}
	m["first"] = "John"
	m["last"] = "Doe"
	fmt.Println(m)

	m = map[string]string{
		"first": "Jane",
		"last":  "Doe",
	}
	fmt.Println(m)

	m = make(map[string]string)
	fmt.Println(m)
	m["first"] = "John"
	m["last"] = "Doe"
	fmt.Println(m)
	fmt.Println(m["first"])
}
