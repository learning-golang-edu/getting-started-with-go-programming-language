package main

import "fmt"

type Person struct {
	First string
	Last  string
	Age   int
	Phone Phone
}

type Phone struct {
	AreaCode string
	Prefix   string
	Suffix   string
}

func main() {
	p := Person{
		First: "John",
		Last:  "Doe",
		Age:   42,
		Phone: Phone{
			AreaCode: "123",
			Prefix:   "345",
			Suffix:   "0123",
		},
	}
	fmt.Println(p)

	q := &Person{"Jane", "Doe", 25, Phone{"123", "456", "7890"}}
	fmt.Println(q)
	fmt.Println(p.Age)
	fmt.Println(q.Age)

	pt := struct {
		X int
		Y int
	}{
		X: 10,
		Y: 20,
	}
	fmt.Println(pt)
}
