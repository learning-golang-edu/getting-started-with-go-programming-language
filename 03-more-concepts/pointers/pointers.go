package main

import "fmt"

func main() {
	s := "My String"
	sptr := &s
	fmt.Println(s)
	fmt.Println(sptr)  // prints address
	fmt.Println(*sptr) // prints stored value

	sptr1 := new(string)
	fmt.Println(sptr1)
	fmt.Println(*sptr1)

	var sptr2 *string
	fmt.Println(sptr2)
	sptr2 = &s
	fmt.Println(sptr2)
	fmt.Println(*sptr2)

	i := new(int)
	fmt.Println(i)
	fmt.Println(*i)
}
