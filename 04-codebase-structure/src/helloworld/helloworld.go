package helloworld

import "golang.org/x/sys/unix"

//GetHelloWorld returns hello world string
func GetHelloWorld() string {
	return "Hello, World!"
}

//GetUserID gets teh ID of the current user
func GetUserID() int {
	return unix.Getuid()
}

//GetAbsValue returns absolute value of given number
func GetAbsValue(i int) int {
	if i > 0 {
		return i
	}
	return -1 * i
}
