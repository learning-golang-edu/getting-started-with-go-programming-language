package helloworld

import (
	"golang.org/x/sys/unix"
	"testing"
)

func TestGetHelloWorld(t *testing.T) {
	result := GetHelloWorld()
	if result != "Hello, World!" {
		t.Fail()
	}
}

func TestGetUserID(t *testing.T) {
	result := GetUserID()
	expected := unix.Getuid()
	if result != expected {
		t.Fail()
	}
}

// to skip this test run command: go test -short
func TestGetAbsValue(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	result := GetAbsValue(-5)
	if result != 5 {
		t.Fail()
	}
	result = GetAbsValue(10)
	if result != 10 {
		t.Fail()
	}
}
