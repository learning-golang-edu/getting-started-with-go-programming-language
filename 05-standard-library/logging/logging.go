package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	log.Print("Hello, World!")
	// log.Panic("oops!") // will print panic message and exit
	// log.Fatalln("oops!") // will print a message and exit

	prefix := fmt.Sprintf("%s: ", os.Args[0])
	info, err := os.Create("info.log")
	if err != nil {
		log.Fatalln("failed to create log file")
	}
	infoLog := log.New(info, prefix, log.LstdFlags)
	// infoLog := log.New(os.Stdout, prefix, log.LstdFlags) // standard output
	errorLog := log.New(os.Stderr, prefix, log.LstdFlags)
	var name string
	flag.StringVar(&name, "name", "", "The name to say hello to")
	flag.Parse()

	if name == "" {
		errorLog.Println("No name supplied!")
		flag.Usage()
		os.Exit(1)
	}
	infoLog.Println("Program started")
	infoLog.Printf("Hello, %s\n", name)
	infoLog.Println("Program finished!")
}
