package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	filename := "output.txt"

	// write file
	err := ioutil.WriteFile(filename, []byte("Hello, World!"), 0644)
	if err != nil {
		panic("unable to write file")
	}

	// read file
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("unable to read file")
	}
	fmt.Println(buf)
	fmt.Println(string(buf))
}
