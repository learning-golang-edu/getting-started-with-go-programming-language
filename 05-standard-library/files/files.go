package main

import (
	"fmt"
	"os"
)

func main() {
	// write file
	f, err := os.Create("output.txt")
	if err != nil {
		panic("unable to create file")
	}
	defer f.Close()
	cnt, err := f.WriteString("Hello, World!")
	if err != nil {
		panic("unable to write file")
	}
	fmt.Printf("Wrote %d bytes\n", cnt)

	// read file
	r, err := os.Open("output.txt")
	if err != nil {
		panic("unable to open file")
	}
	defer r.Close()
	buf := make([]byte, 64)
	cnt, err = r.Read(buf)
	if err != nil {
		panic("unable to read file")
	}
	fmt.Printf("Read %d bytes\n", cnt)
	fmt.Println(buf)
	fmt.Println(buf[:cnt])
	fmt.Println(string(buf[:cnt]))
}
