package main

import (
	"flag"
	"fmt"
	"github.com/spf13/viper"
	"log"
)

var username, password string

func useCredentials() {
	fmt.Printf("Connecting to %s at %s\n",
		viper.GetString("webserver.name"),
		viper.GetString("webserver.endpoint"))
	fmt.Printf("Your username is %s\n", viper.GetString("credentials.username"))
	fmt.Printf("Your password is %s\n", viper.GetString("credentials.password"))
}

func main() {
	flag.StringVar(&username, "user", "", "username to use")
	flag.StringVar(&password, "pass", "", "password to use")
	flag.Parse()
	viper.SetDefault("webserver.endpoint", "http://httpbin.org/get")
	viper.SetDefault("webserver.name", "Testing Endpoint")
	viper.AddConfigPath(".")
	viper.SetConfigName("creds")
	_ = viper.ReadInConfig()
	if username != "" {
		viper.Set("credentials.username", username)
	}
	if password != "" {
		viper.Set("credentials.password", password)
	}
	if !viper.IsSet("credentials.username") || !viper.IsSet("credentials.password") {
		log.Fatalln("must provide credentials")
	}
	useCredentials()
}
