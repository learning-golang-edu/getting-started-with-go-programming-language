package main

import "fmt"

func main() {
	// if statement
	doIf(5)
	doIf(-2)
	doIf(42)

	// switch statement
	doSwitch(10)
	doSwitch(5)

	doSwitchMore(10)
	doSwitchMore(9)
	doSwitchMore(1)
	doSwitchMore(5)
}

func doSwitchMore(a int) {
	switch a {
	case 10:
		fmt.Println("Yes!")
	case 9:
		fmt.Println("9")
	case 1:
		fmt.Println("1")
	default:
		fmt.Println("None of these")
	}
}

func doSwitch(a int) {
	switch a == 10 {
	case true:
		fmt.Println("Yes!")
	case false:
		fmt.Println("No!")
	}
}

func doIf(a int) {
	if a < 0 {
		fmt.Println("Your value is negative!")
	} else if a < 10 {
		fmt.Println("Your value is a single digit!")
	} else {
		fmt.Println("Your value has multiple digits!")
	}
}
