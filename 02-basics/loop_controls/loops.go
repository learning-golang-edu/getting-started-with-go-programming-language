package main

import (
	"fmt"
	"time"
)

func main() {
	// traditional loop structure
	for i := 0; i < 4; i++ {
		fmt.Println(i)
	}

	// infinite for loop
	for {
		fmt.Println("Tick")
		time.Sleep(1 * time.Second)
		break
	}

	// loop with range
	for _, i := range []int{1, 2, 3, 4} {
		fmt.Println(i)
	}

	// for loop as while loop
	a := 1
	for a < 4 {
		fmt.Println(a)
		a++
	}
}
